%Created By Elliot Cramer at Langley NASA
function [data volumeSize] = read_ct_vol2(fileName,numSlice)
% reads in one slice of ct data set
% slice is in the x-y plane
% numSlice is the is the z position of the slice
% use: data = read_ct_vol2(filename, numSlice) or
% [data volumeSize] = read_ct_vol2(fileName,numSlice)
% where data is the slice image
% and volumeSize gives the size of the volume( number of elements in x, y
% and z direction)
% if numSlice is not included, then the center slice of the volume is
% returned.

if (isstruct(fileName))
    fileName = fileName.name;
end
%dir(strrep(fileName,'.vol','.vgi'))
[fid  message]= fopen(strrep(fileName,'.vol','.vgi'),'r');

roiText = fread(fid,1000,'char');
%disp((roiText(1:40)'))
fclose(fid);

data=0;
%%
[nx ny nz]=findNumber(roiText,2);
volumeSize = [nx ny nz];

fid = fopen(fileName,'r','ieee-le');

fnum=0;
if ( nargin() > 1 )
    status=fseek(fid,nx*ny*4*(numSlice-1),'bof');
    if ( status == -1)
        ferror(fid)
    end
end
for i = 1:1
    if ( nargin() == 1 )
        fseek(fid,nx*ny*4*fix(nz/2),'cof');
    end
    [temp num]=fread(fid,nx*ny,'float');
    if (num == nx*ny)
        data = reshape(temp,nx,ny);
    end
    fnum=fnum+1;
    if ( nargin() > 1 )
        break;
    end
end
fclose(fid);
%%
function [nx ny nz]=findNumber(arrayChar,pos)

%%
nx=0;
ny=0;nz=0;
%find carriage return
index = find(arrayChar == 13);

%find equal sign in part of string of interest
numStr = arrayChar((index(pos)+1):(index(pos+1))-1);

p=find(numStr==61);
p2 = find(numStr((p+1):length(numStr))==32)+p;

nx=str2double(char(numStr((p+1):p2(2))'));
ny=str2double(char(numStr(p2(2):p2(3))'));
nz=str2double(char(numStr(p2(3):length(numStr))'));