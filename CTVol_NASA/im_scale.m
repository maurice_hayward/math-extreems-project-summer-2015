%Created By Elliot Cramer at Langley NASA

function data_out = im_scale ( data_in, new_min_in, new_max_in)
% use: data_out = im_scale(data_in) or data_out = im_scale(data_in)
% if new_min_in and new_max_in are input then
% data_out is scaled to (new_min_in to new_max_in)
% else data_out is scaled from zero to one

if ( nargin < 1 )
    help im_scale
    return
end

new_min = 0;
new_max = 1;
if ( nargin > 2 )
    new_min = new_min_in;
    new_max = new_max_in;
end

dr =im_min_max(data_in);
data_out = new_min+(new_max-new_min)*(data_in-dr(1))/(dr(2)-dr(1));

