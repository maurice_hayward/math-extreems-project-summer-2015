%Created By Elliot Cramer at Langley NASA

function dimage = im_limit(dimage_in,dmin, dmax)
% use: image_out = im_limit(image_in, min_in, max_in)
% clips the upper and lower values in the file
% sets all values of image_in less than min_in to min_in
% sets all values of image_in greater than max_in to max_in

if ( nargin < 3 )
    help im_limit
    return
end

%%
dimage = dimage_in;
s = size(dimage);
npts = prod(s);
%%
dimage = reshape(dimage,npts,1);
index_1 = find(dimage < dmin);
if ( prod(size(index_1)) > 0 )
    dimage(index_1) = dmin;
end

index_2 = find(dimage > dmax);
if ( prod(size(index_2)) > 0 )
    dimage(index_2) = dmax;
end
dimage = reshape(dimage,s(1),s(2));