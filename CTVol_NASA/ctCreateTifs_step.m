%Created By Elliot Cramer at Langley NASA
function ctCreateTifs_step( dirName, filePre, data, direction, step,limits, resolution )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if (~exist(dirName,'dir'))
    mkdir(dirName)
end

p = strfind(filePre,'/');
if ( isempty(p) )
    filePre = ['/' filePre];
else
    if (p(1) ~= 1 )
        filePre = ['/' filePre];
    end
end 
s = size(data);

for i =1:step:s(direction)
    if ( direction == 3 )
        cdata = im_scale(im_limit(reshape(data(:,:,i),s(1),s(2)),limits(1),limits(2)));
    end
    if ( direction == 2 )
        cdata = im_scale(im_limit(reshape(data(:,i,:),s(1),s(3)),limits(1),limits(2)));
    end
    if ( direction == 1 )
        cdata = im_scale(im_limit(reshape(data(i,:,:),s(2),s(3)),limits(1),limits(2)));
    end
   imwrite(cdata, [dirName filePre num2str(i*resolution,'%6.6d.tif')])
end

end

