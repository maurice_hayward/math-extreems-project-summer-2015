%Created By Elliot Cramer at Langley NASA
function vol = ctLoadVolume(fileName, range)
% ctLoadVolume - read in either part or all of ct volume
% vol = ctLoadVolume(fileName, range)
% range is optional - if length of 2 extracts slices range(1) range(2)
% if range length is 5 extract in the x direction range(1) to range(2)
% and in the z direction range(3) to range(4)
% if range length is 6, extract part of volume defined by range, x
% direction first, y second and z third

[~, ctSize] = read_ct_vol2(fileName,1);
i1 = 1;
i2 = ctSize(1);
j1 = 1;
j2 = ctSize(2);
k1 = 1;
k2 = ctSize(3);

if ( nargin() > 1 )
   if(length(range)> 3 )
       i1=range(1);
       i2=range(2);
       k1=range(3);
       k2=range(4);
   end
   if(length(range)> 5 )
       j1=range(3);
       j2=range(4);
       k1=range(5);
       k2=range(6);
   end
   if( length(range)==2) 
       k1=range(1);
       k2=range(2);
   end
end

nz = k2+1-k1;
ny = j2+1-j1;
nx = i2+1-i1;
vol = zeros(nx,ny,nz);

for i = 1:nz
    slice = read_ct_vol2(fileName,i+k1-1);
    vol(:,:,i) = slice(i1:i2,j1:j2); 
end



end

