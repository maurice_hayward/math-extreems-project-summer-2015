function  bettiNum = lifespan_threshing(lifespans, lifespanT)

lifespans  = lifespans(lifespans > lifespanT);
bettiNum =  length(lifespans);