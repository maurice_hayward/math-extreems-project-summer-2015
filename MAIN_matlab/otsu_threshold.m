function  threshold = otsu_threshold(hist)
%Mandtory Parameter: hist- Histogram of lifespan or image values

%Total number of pixels
total = sum(hist); % total number of values

max_T =  length(hist);

T = 1:max_T;

sums = sum(T.*hist);


sumB = 0;
wB = 0;
wF = 0;

varMax = 0;
threshold = 0;

for t = T
    wB = wB + hist(t); %weigth Background
    if(wB == 0) 
        continue;
    end
    
    wF = total - wB; %weigth Forground
    if(wF == 0); 
        break;
    end;
    
    sumB = sumB + (t * hist(t));
    
    mB = sumB / wB;    %mean background
    mF = (sums - sumB) / wF;% mean background
    
    varBetween = wB * wF * (mB -mF)*(mB - mF);
    
    if(varBetween > varMax)
        varMax = varBetween;
        threshold = t;
    end
end

    
    






