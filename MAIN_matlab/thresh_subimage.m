function sub_BD_mat = thresh_subimage(BD, imageT)
%Mandtory Input- BD = Birth vs death matrix
%Mandtory Input - ImageT = given Image threhold 
%OUTPUT - Beath vs death matix that within givein threshold

f1 = find(BD(:,1) <= imageT);
f2 = find(BD(:,2) > imageT);
Tindex = intersect(f1,f2);
sub_BD_mat = BD(Tindex,:);