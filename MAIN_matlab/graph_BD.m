function  graph_BD(fname, imageT, lifespanT,bettiNum, maxx)
%Graphs Birth V. Death graph and Theshold lines
%fname - is the persueus create file 
%imageT - is the image Threshold - if 0 No Image Thershold lines will be plotted
%lifespanT - is the lifespan Threshold - if 0 No Lifespan Thershold lines will be plotted

%% Plot Birth vs Death graph
persdia(fname);

titles = fname;

hold on
%Plot Thershold lines
if imageT
    
    plot( [imageT,imageT],[0, maxx], 'k--', [0,maxx], [imageT,imageT], 'k--');
    titles = strcat( titles, ' T = ',num2str(imageT));
end

%Plot Lifepan line 
if lifespanT
    x= 1:maxx;
    y = x + lifespanT;
    plot(x,y,'r--');
    titles = strcat(titles, ' L = ',num2str(lifespanT));
end 
%tille graph
titles = strcat(titles, ' \beta = ', num2str(bettiNum));
title(titles);
hold off