%% function params
function  ls_hist = lifespanHist(lifespans)
%Make and returns histogram of lifespans

%mandatory pararmter - lifespans - list of lifespans

figure;
max_lifespan = max(lifespans);
h = histogram(lifespans,'BinLimits', [1,max_lifespan],'BinMethod', 'integers');
ls_hist = h.Values; %histogram of Lifespans
axis = gca; axis.YScale = 'log';
title('Histogram of Lifespans')
xlabel('Lifespan values');
ylabel('log of the number of compoents');









    
    


