function  threshold = entropy_threshold(hist)
%Mandtory Parameter: hist- Histogram of lifespans 


%YM's comment:
%   Make sure the persisted generators are EXCLUDED from hist.
%   It is because in Perseus, they store as "1 -1", which may cause
%   problems.

hist = hist + 1;%YM's comment.
                %To get around p being 0, we artificially add 1 to the
                %histogram.  This way ensures that p is strictly positive.
            

N_2 = sum(hist);
p = hist/N_2;  %add 1 to not get Nans
               %YM's comment:
               %    p is the probability distribution
               %    Hence, the values should be between 0 and 1.
               %    Adding 1 to p that breaks assumption on the algorithm.

H_m = -sum(p .* log( p));
P_m = sum(hist);

max_span = length(hist) - 1;
y = zeros([1, max_span]);
for s = 1:max_span
  y(s) =  entropy_fun(s,p,H_m,P_m);
end
[maxval, threshold] = max(y);
%YM's Comment:
%You don't want the max value. Instead, you want the INDEX where max occurs.
%mathematically, it is called (arg max).
end


function phi = entropy_fun(S,p,H_m,P_m)
p = p(1:S);
H_s = - sum (p.*log(p));
P_s = sum(p);


phi = log(P_s *(1 - P_s)) + H_s/P_s + ( H_m - H_s)/(1 - P_s);
end
