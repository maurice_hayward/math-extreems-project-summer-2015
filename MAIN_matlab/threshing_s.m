  
%% Intialize stuff
clc;    % Clear command window.
clear;  % Delete all variables.
workspace;      % Make sure the workspace panel is showing.
fontSize = 20;

%% Get Frame
range = [ 30, 1045, 30, 1045, 150, 151]; %crops the orginal data from x1 = 30 to x2 = 1045 and y1 = 30 to y2 = 1045. Looking at frames 150 to 151
frame = ctLoadVolume('../NESB-crimp.vol', range ); %  assume raw xray data in pervious directory
fname = 'frame150_151';

%% Scale images by 10
frame = scale_image(frame,10)
max_v = max(max(frame)); % max value of frame 

%% Histogram of Pixel Intentisy

%histogram of frame
figure;

hold on
h = histogram(frame,'BinLimits', [1,max_v],'BinMethod', 'integers');
hist = h.Values; % histograme of frame 
axis = gca; axis.YScale = 'log'; %Change Y-axis to log scale
title('Histogram of pixel intensity of frame ')
xlabel('pixel values');
ylabel('log of the number of compoents');
hold off

%% Image Theshold with Ostu Method
otsu_T = otsu_threshold(hist)
BW_otsu = ~(frame <= otsu_T); %Image Thresholding
figure;
imshow(BW_otsu);

%% Image Theshold with Entropy
entropy_T = entropy_threshold(hist)
BW_entropy = ~(frame <= entropy_T); %Image Thresholding
figure;
imshow(BW_entropy);

%% Persus ( Persistent Homology)
runPerseus(fname, frame, 3); %assumes All Perseus files are in current directory 

%% Make Birth/death mat, calc LifeSpan  and Lifespan Histogram for H_0 
text_0 = strcat(fname,'_0.txt'); % gives you "fname_0.txt" so look at Betti zero info
BD_0 = make_BirthDeath(text_0);    % make Birth v Death chart for Betti 0
lifespans_0 = BD_0(:,2) - BD_0(:,1);     % list of all lifespans
lifespan_hist_0 = lifespanHist(lifespans_0);

%% Lifespan Threshold for H_0 - Otsu Mthod and Entropy
LS_otsu_0 =  otsu_threshold(lifespan_hist_0)

LS_entropy_0 = entropy_threshold(lifespan_hist_0)

%% Birth v Death for H_0 - No theshold, Ostu , and Entropy thershold
betti = calc_betti(text_0,BD_0,max_v,0,0);

betti_ostu = calc_betti(text_0,BD_0,max_v,0,LS_otsu_0);

betti_entropy =  calc_betti(text_0,BD_0,max_v,0,LS_entropy_0);


% %% Make Birth/death mat, calc LifeSpan  and Lifespan Histogram for H_1 
% text_1 = strcat(fname,'_1.txt');
% BD_1 = make_BirthDeath(text_1);
% lifespans_1 = BD_1(:,2) - BD_1(:,1);
% lifespan_hist_1 = lifespanHist(lifespans_1);
% 
% %% Lifespan Threshold for H_1 - Otsu Mthod and Entropy
% LS_otsu_1 =  otsu_threshold(lifespan_hist_1)
% 
% LS_entropy_1 = entropy_threshold(lifespan_hist_1)
% 
% %% Birth v Death for H_1 - No theshold, Ostu , and Entropy thershold
% betti = calc_betti(text_1,BD_1,max_v,0,0);
% 
% betti_ostu = calc_betti(text_1,BD_1,max_v,0,LS_otsu_1);
% 
% betti_entropy =  calc_betti(text_1,BD_1,max_v,0,LS_entropy_1);
















