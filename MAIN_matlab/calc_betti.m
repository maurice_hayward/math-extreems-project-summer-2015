function BettiNum = calc_betti(fname, BD, max_T, imageT, lifespanT)

%% Image Threholds Birth/ death graph 
if imageT
    BD = thresh_subimage(BD,imageT);
end

%% Theshold lifespans;
lifespans = BD(:,2) - BD(:,1); % calculate lifespans
if lifespanT
    lifespans = lifespans( lifespans > lifespanT); %threshold lifespans
end

BettiNum = length(lifespans); %Betti Number is just the number of lifespan generators 

graph_BD(fname,imageT, lifespanT, BettiNum, max_T); %makes Birth v Death graph
