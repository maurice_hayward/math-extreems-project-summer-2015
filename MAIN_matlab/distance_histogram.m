function dist_hist = distance_histogram(BD, LS_T)
%Mandortaory Parameter - BD - Birth/Death matrix
%Mandortaory Parameter - LS_T - Lifespan threshold

dist_hist = abs(BD(:,2) - BD(:,1) - LS_T)/sqrt(2);


