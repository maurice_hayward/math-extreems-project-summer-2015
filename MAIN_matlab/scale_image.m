function scaled = scale_image(im, factor)
%mandtory parameter- im -  a grayscaled image
%mandtory parameter - factor- the integer scale image vales


%rescale image values to have new range [1, new_max]

b = int32(im .* factor);
scaled = abs(min(b(:))) + 1 + b;

