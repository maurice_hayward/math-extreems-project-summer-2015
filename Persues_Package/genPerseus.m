function genPerseus(mat, fname, dim)
%Input: 
%   mat: data in terms of 3D matrix
%   fname: file name for the perseus output
    'conversion...'
    a = convTo1darray(mat);
    [row, col, h] = size(mat);
    if dim == 3
        a = [3;row;col; h; a];
    else
        a = [2;row;col;a];
    end
    'write in file...'
    %dlmwrite (fname, a, '\n');
    fid = fopen(fname, 'wt');
    for i=1:length(a)
        fprintf(fid, '%g\n', a(i));
    end
    fclose(fid);
end